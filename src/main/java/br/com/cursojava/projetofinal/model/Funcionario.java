package br.com.cursojava.projetofinal.model;

import java.time.LocalDate;

public class Funcionario {

    /** func_code INTEGER PRIMARY KEY AUTO_INCREMENT */
    private int matricula;

    /** func_name VARCHAR(30) */
    private String nome;

    /** func_adm_date DATE */
    private LocalDate dataAdmissao;

    /** func_rmnt_val DOUBLE */
    private double salario;

    /** role_code INTEGER */
    private int codigoCargo;

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(LocalDate dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getCodigoCargo() {
        return codigoCargo;
    }

    public void setCodigoCargo(int codigoCargo) {
        this.codigoCargo = codigoCargo;
    }
}
