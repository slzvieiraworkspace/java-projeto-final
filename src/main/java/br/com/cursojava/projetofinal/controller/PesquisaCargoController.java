package br.com.cursojava.projetofinal.controller;

import java.util.List;

import br.com.cursojava.projetofinal.model.Cargo;
import br.com.cursojava.projetofinal.repository.CargoRepository;
import br.com.cursojava.projetofinal.repository.RepositoryException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class PesquisaCargoController extends AbstractController {

	@FXML
	private TextField txtNomeCargo;

	@FXML
	private TableView<Cargo> tabCargo;

	@FXML
	private TableColumn<Cargo, Integer> colId;

	@FXML
	private TableColumn<Cargo, String> colNome;

	@FXML
	void initialize() {
		colId.setCellValueFactory(new PropertyValueFactory<>("codigo"));
		colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
	}

	@FXML
	void procurarOnAction(ActionEvent event) {

		try {

			tabCargo.getItems().clear();

			String nomeCargo = txtNomeCargo.getText();
			CargoRepository repo = new CargoRepository();

			List<Cargo> lista = repo.findByName(nomeCargo);

			tabCargo.getItems().addAll(lista);

		} catch (RepositoryException e) {

			alertErro(e.getMessage());
			e.printStackTrace();
		}
	}
}
