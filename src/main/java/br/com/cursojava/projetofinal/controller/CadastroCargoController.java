package br.com.cursojava.projetofinal.controller;

import br.com.cursojava.projetofinal.model.Cargo;
import br.com.cursojava.projetofinal.repository.CargoRepository;
import br.com.cursojava.projetofinal.repository.RepositoryException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class CadastroCargoController extends AbstractController {

    @FXML
    private TextField txtNome;

    @FXML
    void salvarOnAction(ActionEvent event) {

        try {

            if (txtNome.getText().length() == 0) {
                alertErro("Digite o nome do cargo");
                return;
            }

            if (txtNome.getText().length() > 20) {
                alertErro("O cargo deve ter no máximo 20 caracteres");
                return;
            }
            
            Cargo cargo = new Cargo();
            cargo.setNome(txtNome.getText());
            
            CargoRepository repo = new CargoRepository();
            repo.save(cargo);

            limparOnAction(event);
            alertSucesso("Cargo cadastrado com sucesso.");

        } catch (RepositoryException e) {

            alertErro(e.getMessage());
            e.printStackTrace();
        }
    }

    @FXML
    void limparOnAction(ActionEvent event) {
        txtNome.setText("");
        txtNome.requestFocus();
    }
}
