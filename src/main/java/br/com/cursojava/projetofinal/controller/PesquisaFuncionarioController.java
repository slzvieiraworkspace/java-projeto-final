package br.com.cursojava.projetofinal.controller;

import java.util.List;

import br.com.cursojava.projetofinal.model.Funcionario;
import br.com.cursojava.projetofinal.repository.FuncionarioRepository;
import br.com.cursojava.projetofinal.repository.RepositoryException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class PesquisaFuncionarioController extends AbstractController {

    @FXML
    private TextField txtNome;

    @FXML
    private TableView<Funcionario> tabFuncionario;

    @FXML
    private TableColumn<Funcionario, Integer> colMatricula;

    @FXML
    private TableColumn<Funcionario, String> colNome;

    @FXML
    private TableColumn<Funcionario, Double> colSalario;

    @FXML
    void initialize() {
    	colMatricula.setCellValueFactory(new PropertyValueFactory<>("matricula"));
    	colNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
    	colSalario.setCellValueFactory(new PropertyValueFactory<>("salario"));
    }
    
    @FXML
    void procurarOnAction(ActionEvent event) {

		try {

			tabFuncionario.getItems().clear();

			String nome = txtNome.getText();
			FuncionarioRepository repo = new FuncionarioRepository();

			List<Funcionario> lista = repo.findByName(nome);

			tabFuncionario.getItems().addAll(lista);

		} catch (RepositoryException e) {

			alertErro(e.getMessage());
			e.printStackTrace();
		}
    }
}
