package br.com.cursojava.projetofinal.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public abstract class AbstractController {

    @FXML
    protected void alertSucesso(String mensagem) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Projeto Final");
        alert.setHeaderText("Sucesso");
        alert.setContentText(mensagem);
        alert.show();
    }

    @FXML
    protected void alertErro(String mensagem) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Projeto Final");
        alert.setHeaderText("Erro");
        alert.setContentText(mensagem);
        alert.show();
    }
}
