package br.com.cursojava.projetofinal.controller;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;

import br.com.cursojava.projetofinal.model.Cargo;
import br.com.cursojava.projetofinal.model.Funcionario;
import br.com.cursojava.projetofinal.repository.CargoRepository;
import br.com.cursojava.projetofinal.repository.FuncionarioRepository;
import br.com.cursojava.projetofinal.repository.RepositoryException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class CadastroFuncionarioController extends AbstractController {

    @FXML
    private TextField txtNome;

    @FXML
    private ComboBox<Cargo> cmbCargo;

    @FXML
    private DatePicker datAdmissao;

    @FXML
    private TextField txtSalario;

    @FXML
    void initialize() {
        
        try {

            CargoRepository repo = new CargoRepository();
            List<Cargo> lista = repo.findAll();
            cmbCargo.getItems().addAll(lista);

        } catch (RepositoryException e) {

            alertErro(e.getMessage());
            e.printStackTrace();
        }
    }

    @FXML
    void salvarOnAction(ActionEvent event) {

        Funcionario funcionario = new Funcionario();
        
        /* Valida e coleta da tela o nome digitado */
        if (txtNome.getText().length() > 30) {
            alertErro("O nome do funcionário deve ter no máximo 30 caracteres");
            return;
        }
        if (txtNome.getText().length() == 0) {
            alertErro("Nome é obrigatório");
            return;
        }
        funcionario.setNome(txtNome.getText());
        
        /* Valida e coleta da tela o cargo selecionado */
        if (cmbCargo.getSelectionModel().getSelectedIndex() < 0) {
            alertErro("Cargo é obrigatório");
            return;
        }
        funcionario.setCodigoCargo(cmbCargo.getSelectionModel().getSelectedItem().getCodigo());
        
        /* Valida e coleta da tela a data de admissao */
        if (datAdmissao.getValue() == null) {
            alertErro("Data de admissão é obrigatória");
            return;
        }
        funcionario.setDataAdmissao(datAdmissao.getValue());
        
        /* Valida e coleta o salário digitado */
        try {
            DecimalFormat mascaraNumerica = new DecimalFormat("#,##0.00");
            funcionario.setSalario(mascaraNumerica.parse(txtSalario.getText()).doubleValue());
        } catch (ParseException e) {
            alertErro("Salario inválido");
            return;
        }
        
        try {

            FuncionarioRepository repo = new FuncionarioRepository();
            repo.save(funcionario);
            
            limparOnAction(event);
            alertSucesso("Dados gravados com sucesso.");

        } catch (RepositoryException e) {

            alertErro(e.getMessage());
            e.printStackTrace();
        }
    }
    
    @FXML
    void limparOnAction(ActionEvent event) {
        txtNome.setText("");
        cmbCargo.getSelectionModel().clearSelection();
        datAdmissao.setValue(null);
        txtSalario.setText("");
        txtNome.requestFocus();
    }
}
