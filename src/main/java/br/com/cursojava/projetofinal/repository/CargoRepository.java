package br.com.cursojava.projetofinal.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.cursojava.projetofinal.model.Cargo;

public class CargoRepository extends AbstractRepository {

    public void save(Cargo cargo) throws RepositoryException {

        Connection cn = null;
        PreparedStatement ps = null;

        try {

            cn = getConnection();
            ps = cn.prepareStatement("INSERT INTO tab_role (role_name) VALUES (?)");
            ps.setString(1, cargo.getNome());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RepositoryException("Falha ao salvar dados do cargo.", e);
        } finally {
            closeResources(cn, ps, null);
        }
    }

    public List<Cargo> findAll() throws RepositoryException {

        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;

        try {

            cn = getConnection();
            st = cn.createStatement();
            rs = st.executeQuery("SELECT role_code, role_name FROM tab_role ORDER BY role_name");

            List<Cargo> lista = new ArrayList<>();
            
            while (rs.next()) {
                Cargo cargo = new Cargo();
                cargo.setCodigo(rs.getInt("role_code"));
                cargo.setNome(rs.getString("role_name"));
                lista.add(cargo);
            }
            
            return lista;

        } catch (SQLException e) {
            throw new RepositoryException("Falha ao obter lista de cargos", e);
        } finally {
            closeResources(cn, st, rs);
        }
    }

	public List<Cargo> findByName(String nomeCargo) throws RepositoryException {

		Connection cn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {

            cn = getConnection();
            ps = cn.prepareStatement("SELECT role_code, role_name FROM tab_role WHERE role_name LIKE ? ORDER BY role_name");
            ps.setString(1, nomeCargo);
            rs = ps.executeQuery();

            List<Cargo> lista = new ArrayList<>();
            
            while (rs.next()) {
                Cargo cargo = new Cargo();
                cargo.setCodigo(rs.getInt("role_code"));
                cargo.setNome(rs.getString("role_name"));
                lista.add(cargo);
            }
            
            return lista;

        } catch (SQLException e) {
            throw new RepositoryException("Falha ao obter lista de cargos", e);
        } finally {
            closeResources(cn, ps, rs);
		}
	}
}
