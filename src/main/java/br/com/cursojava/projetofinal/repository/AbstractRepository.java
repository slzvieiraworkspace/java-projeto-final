package br.com.cursojava.projetofinal.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractRepository {

    public Connection getConnection() throws SQLException {

        return DriverManager.getConnection("jdbc:mysql://remotemysql.com:3306/Y4bUAh9una?useSSL=false", "Y4bUAh9una", "GILHq5Wcr7");
    }

    protected void closeResources(Connection cn, Statement ps, ResultSet rs) {

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }

        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }

        if (cn != null) {
            try {
                cn.close();
            } catch (SQLException e) {
            }
        }
    }
}
