package br.com.cursojava.projetofinal.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.cursojava.projetofinal.model.Funcionario;

public class FuncionarioRepository extends AbstractRepository {

    public void save(Funcionario funcionario) throws RepositoryException {

        Connection cn = null;
        PreparedStatement ps = null;
        
        try {

            cn = getConnection();
            ps = cn.prepareStatement("INSERT INTO tab_func (func_name, func_adm_date, func_rmnt_val, role_code) VALUES (?, ?, ?, ?)");
            ps.setString(1, funcionario.getNome());
            ps.setDate(2, Date.valueOf(funcionario.getDataAdmissao()));
            ps.setDouble(3, funcionario.getSalario());
            ps.setInt(4, funcionario.getCodigoCargo());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RepositoryException("Falha ao salvar dados do funcionário", e);
        } finally {
            closeResources(cn, ps, null);
        }
    }

	public List<Funcionario> findByName(String nome) throws RepositoryException {

		Connection cn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {

			cn = getConnection();
			ps = cn.prepareStatement("SELECT func_code, func_name, func_adm_date, func_rmnt_val, role_code FROM tab_func WHERE func_name LIKE ? ORDER BY func_name");
			ps.setString(1, nome);
			rs = ps.executeQuery();

			List<Funcionario> lista = new ArrayList<>();

			while (rs.next()) {

				Funcionario func = new Funcionario();

				func.setMatricula(rs.getInt("func_code"));
			    func.setNome(rs.getString("func_name"));
			    func.setDataAdmissao(rs.getDate("func_adm_date").toLocalDate());
			    func.setSalario(rs.getDouble("func_rmnt_val"));
			    func.setCodigoCargo(rs.getInt("role_code"));

				lista.add(func);
			}
			
			return lista;

		} catch (SQLException e) {
            throw new RepositoryException("Falha ao obter lista de funcionários", e);
        } finally {
            closeResources(cn, ps, rs);
		}
	}
}
